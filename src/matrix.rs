use keebrs::{
    debounce::DebounceState,
    matrix::{
        Matrix,
        Pull,
        Read,
    },
    stateful,
};
use stm32f1xx_futures::{
    self,
    hal::gpio::{
        gpioa::{
            PA10,
            PA6,
            PA7,
            PA8,
            PA9,
        },
        gpiob::{
            PB0,
            PB3,
            PB4,
            PB8,
        },
        gpioc::{
            PC13,
            PC14,
            PC15,
        },
        Input,
        Output,
        PullUp,
        PushPull,
    },
};

pub type Scanner = Matrix<
    (
        PB3<Output<PushPull>>,
        PB8<Output<PushPull>>,
        PC15<Output<PushPull>>,
        PC14<Output<PushPull>>,
        PC13<Output<PushPull>>,
    ),
    (
        PA7<Input<PullUp>>,
        PB0<Input<PullUp>>,
        PA6<Input<PullUp>>,
        PA8<Input<PullUp>>,
        PA9<Input<PullUp>>,
        PA10<Input<PullUp>>,
        PB4<Input<PullUp>>,
    ),
>;

pub type StatefulScanner<S, D> =
    stateful::StatefulScanner<S, [[DebounceState; Scanner::NREAD]; Scanner::NPULL], D>;

macro_rules! build_scanner {
    ($gpioa:expr, $gpiob:expr, $gpioc:expr) => {
        Matrix {
            pull: (
                $gpiob.pb3.into_push_pull_output(&mut $gpiob.crl),
                $gpiob.pb8.into_push_pull_output(&mut $gpiob.crh),
                $gpioc.pc15.into_push_pull_output(&mut $gpioc.crh),
                $gpioc.pc14.into_push_pull_output(&mut $gpioc.crh),
                $gpioc.pc13.into_push_pull_output(&mut $gpioc.crh),
            ),
            read: (
                $gpioa.pa7.into_pull_up_input(&mut $gpioa.crl),
                $gpiob.pb0.into_pull_up_input(&mut $gpiob.crl),
                $gpioa.pa6.into_pull_up_input(&mut $gpioa.crl),
                $gpioa.pa8.into_pull_up_input(&mut $gpioa.crh),
                $gpioa.pa9.into_pull_up_input(&mut $gpioa.crh),
                $gpioa.pa10.into_pull_up_input(&mut $gpioa.crh),
                $gpiob.pb4.into_pull_up_input(&mut $gpiob.crl),
            ),
        }
    };
}
