pub fn reboot_bootloader(_: crate::hal::backup_domain::BackupDomain) -> ! {
    unsafe {
        // Note: Using raw offsets here because it's wrong in the upstream stm32f1 crate.
        const BKP_BASE: usize = 0x4000_6C00;
        const BKP_DR10_OFF: usize = 0x28;
        core::ptr::write_volatile(core::mem::transmute(BKP_BASE + BKP_DR10_OFF), 0x424C)
    };
    reboot();
}

pub fn reboot() -> ! {
    let scb: &mut crate::stm32::SCB = unsafe { core::mem::transmute(crate::stm32::SCB::ptr()) };
    scb.system_reset()
}
